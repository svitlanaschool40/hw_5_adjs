class Card {
  constructor(postId, title, text, userName, userLastName, userEmail) {
    this.postId = postId;
    this.title = title;
    this.text = text;
    this.userName = userName;
    this.userLastName = userLastName;
    this.userEmail = userEmail;
  }

  createCardElement() {
    const cardElement = document.createElement('div');
    cardElement.classList.add('card');

    const deleteButton = document.createElement('span');
    deleteButton.classList.add('delete-button');
    deleteButton.innerHTML = '&#10006;';

    const titleElement = document.createElement('h4');
    titleElement.textContent = this.title;

    const textElement = document.createElement('p');
    textElement.textContent = this.text;

    const userElement = document.createElement('p');
    userElement.textContent = `${this.userName} ${this.userLastName} (${this.userEmail})`;

    cardElement.appendChild(deleteButton);
    cardElement.appendChild(titleElement);
    cardElement.appendChild(textElement);
    cardElement.appendChild(userElement);

    deleteButton.addEventListener('click', () => {
      this.deleteCard();
    });

    return cardElement;
  }

  deleteCard() {
    fetch(`https://ajax.test-danit.com/api/json/posts/${this.postId}`, {
      method: 'DELETE'
    })
      .then(response => {
        if (response.ok) {
          const cardElement = document.getElementById(`card-${this.postId}`);
          cardElement.remove();
        }
      })
      .catch(error => console.log(error));
  }
}

async function loadNewsFeed() {
  const usersResponse = await fetch('https://ajax.test-danit.com/api/json/users');
  const usersData = await usersResponse.json();

  const postsResponse = await fetch('https://ajax.test-danit.com/api/json/posts');
  const postsData = await postsResponse.json();

  const newsFeedElement = document.getElementById('news-feed');

  postsData.forEach(post => {
    const { title, text, userId } = post;
    const { name, surname, email } = usersData.find(user => user.id === userId);

    const card = new Card(post.id, title, text, name, surname, email);
    const cardElement = card.createCardElement();
    cardElement.id = `card-${post.id}`;

    newsFeedElement.appendChild(cardElement);
  });
}

loadNewsFeed().catch(error => console.log(error));